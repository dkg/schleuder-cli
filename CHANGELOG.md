Change Log
==========

This project adheres to [Semantic Versioning](http://semver.org/).

The format of this file is based on [Keep a Changelog](http://keepachangelog.com/).

## [0.0.3] / 2017-01-26

### Changed

* Always use TLS.
* Required version of schleuder: 3.0.0.


## [0.0.2] / 2017-01-25

### Changed

* Always send API-key.
* Required version of schleuder: 3.0.0rc1.

### Fixed

 * Use section 8 instead of 1 for manpage.
 * Use correct link to schleuder in README.


## [0.0.1] / 2017-01-25

### Changed

 * Use section 1 instead of 8 for manpage.
 * Providing a key for the admin is now optional when creating new lists.

### Fixed

 * Fixed importing binary OpenPGP-keys.

### Added

 * Show a hint if delivery is disabled for a subscription when listing them.


## 0.0.1.beta13

### Changed

 * Flat config file, no nested keys anymore.
 * Fix tarball to contain correct data.
 * Create basedir of config if necessary.

## 0.0.1.beta12

### Changed

 * Write default config if none exists yet, let only user+group access it.


## [before]

### Added

 * Everything.

